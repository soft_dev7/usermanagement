/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.myproject.model.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author KHANOMMECOM
 */
public class UserService { //=> Manage user in system
    private static ArrayList<User> userList = new ArrayList<>(); //static = the only one
    //Mockup Data
    
    static { //static block => It's block will load when class load
        userList.add(new User("admin", "1234"));
        
        
    }
    //Create
    public static boolean addUser(User user){ //Build for check user 
        userList.add(user);
        return true;
    }
    
    public static boolean addUser(String userName, String  password){ //Build for check user 
        userList.add(new User(userName, password));
        return true;
    }
    
    /*Update : It have 2 step for process 
     Step 01 : Pull data in that index to edit
     Step 02 : When you edit data of finish, Data is will update in User
    */
    public static boolean updateUsre(int index,User user){
        userList.set(index, user);
        return true;
    }
    
    //Read 1 user
    public static User getUser(int index){ //get is meaning read data
        if(index > userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    
    //Read all user
    public static ArrayList<User> getUser(){ //get is meaning read data
        return userList;
    }
    //Read all user
    public static ArrayList<User> searchUser(String searchText){ //get is meaning read data
        ArrayList<User> list = new ArrayList<>();
        for(User user: userList){
            if(user.getUsername().startsWith(searchText)){
                list.add(user);
            }
        }
        return userList;
    }
    
    /*Delete : Have 2 type | 
    Type 1 : You can delete form object
    Type 2 : You can delete form index*/
    
    public static boolean delUser(int index){ //delete form index
        userList.remove(index);
        return true;
    }
    
    public static boolean delUser(User user){ //delete form object
        userList.remove(user);
        return true;
    }
    
    public static User login(String userName, String password){
        for(User user : userList){
            if(user.getUsername().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }
    
    public static void save(){
        
    }
    public static void load(){
        
    }
}
