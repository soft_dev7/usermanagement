/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.myproject.model.usermanagement;

/**
 *
 * @author KHANOMMECOM
 */
public class TestUserService {
    public static void main(String[] args) {
        //Add
        UserService.addUser("user2","4321");
        UserService.addUser("user3","54321");
        System.out.println(UserService.getUser());
        
        //Read
        User user = UserService.getUser(3);
        System.out.println(user);
        
        //Update
        user.setPassword("123");
        UserService.updateUsre(3, user);
        System.out.println(UserService.getUser());
        
        //Delete
        UserService.delUser(user);
        System.out.println(UserService.getUser());
        
        //Login
        System.out.println(UserService.login("admin", "1234"));
    }
}
